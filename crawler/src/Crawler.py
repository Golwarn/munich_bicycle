import os
import sys

from Crawler.CounterDataCrawler import CounterDataCrawler
from Crawler.PackageCrawler import PackageCrawler
from Crawler.PackagesCrawler import PackagesCrawler
from Crawler.WeatherDataCrawler import WeatherDataCrawler
from Projection.CounterDataProjection import CounterDataProjection
from Projection.WeatherDataProjection import WeatherDataProjection

argv = sys.argv
db_port = 27017
if "DB_PORT" in os.environ:
    db_port = os.environ["DB_PORT"]
db_host = "localhost"
if "DB_HOST" in os.environ:
    db_host = os.environ["DB_HOST"]
db_user = ""
if "DB_USER" in os.environ:
    db_user = os.environ["DB_USER"]
db_password = ""
if "DB_PASS" in os.environ:
    db_password = os.environ["DB_PASS"]
db_name = "munich_bicycle"
if "DB_NAME" in os.environ:
    db_name = os.environ["DB_NAME"]

if "-h" in argv:
    print("Crawler.py [option]\n")
    print("options:")
    print("\t-h\t\t\thelp")
    print("\t-dbhost\t\tdatabase host")
    print("\t-dbport\t\tdatabase port")
    print("\t-dbuser\t\tdatabase user")
    print("\t-dbpass\t\tdatabase password")
    print("\t-dbname\t\tcollection name")
    exit(0)
if "-dbhost" in argv:
    index = argv.index("-dbhost")
    db_host = argv[index + 1]
if "-dbport" in argv:
    index = argv.index("-dbport")
    db_port = int(argv[index + 1])
if "-dbuser" in argv:
    index = argv.index("-dbuser")
    db_user = int(argv[index + 1])
if "-dbpass" in argv:
    index = argv.index("-dbpass")
    db_password = int(argv[index + 1])
if "-dbname" in argv:
    index = argv.index("-dbname")
    db_name = int(argv[index + 1])


print("start with parameter:")
print("database host: " + db_host)
print("database port: " + str(db_port))

print("packages")
packagesCrawler = PackagesCrawler(database=db_name, host=db_host, port=db_port, user=db_user, password=db_password)
updatedPackages = packagesCrawler.update()
print("package")
packageCrawler = PackageCrawler(database=db_name, host=db_host, port=db_port, user=db_user, password=db_password)
packageCrawler.update(packagesCrawler.get_data())
print("counter")
counterDataCrawler = CounterDataCrawler(database=db_name, host=db_host, port=db_port, user=db_user, password=db_password)
counterDataCrawler.update()
print("weather")
weatherDataCrawler = WeatherDataCrawler(database=db_name, host=db_host, port=db_port, user=db_user, password=db_password)
weatherDataCrawler.update()
print("projection counter")
dataProjection = CounterDataProjection(database=db_name, host=db_host, port=db_port, user=db_user, password=db_password)
dataProjection.update()
print("projection weather")
weatherProjection = WeatherDataProjection(database=db_name, host=db_host, port=db_port, user=db_user,
                                          password=db_password)
weatherProjection.update()

print("updates done")
