import datetime

import pytz as pytz

from Projection.BaseProjection import BaseProjection


class WeatherDataProjection(BaseProjection):
    def __init__(self,
                 database: str = "munich_bicycle",
                 srcCollection: str = "weather_data",
                 host: str = "localhost",
                 port: int = 27017,
                 dstCollection: str = "projection_weather_records",
                 user="",
                 password=""):
        super().__init__(database, srcCollection, host, port, dstCollection, user, password)

    def update(self):
        last_call = self.get_last_call()
        results = self.get_src_collection().find({"date": {"$gt": last_call["date"]}},
                                                 {"data.result.records": 1, "_id": 1, "date": 1})
        for result in results:
            month_of_record = result["date"]
            for record in result["data"]["result"]["records"]:
                date = self.get_date(month_of_record, record)

                start = self.get_time(date, record["uhrzeit_start"])
                timezone = pytz.timezone("Europe/Berlin")

                dto = {
                    "date": start,
                    "dateUtc": start.astimezone(timezone),
                    "sum": int(record["gesamt"]),
                    "direction_1": int(record["richtung_1"]),
                    "direction_2": int(record["richtung_2"]),
                    "counterposition": record["zaehlstelle"],
                    "cloudiness": float(record["bewoelkung"]),
                    "sunshineHoures": float(record["sonnenstunden"]),
                    "precipitations": float(record["niederschlag"]),
                    "minTemp": float(record["min-temp"]),
                    "maxTemp": float(record["max-temp"])
                }
                self.save(dto)

        self.close_connection()
        self.close_src_connection()

    def get_last_call(self):
        last_call = self.load_last()
        if last_call is None:
            last_call = {"date": datetime.datetime(self.START_YEAR, self.START_MONTH, 1)}
        return last_call
