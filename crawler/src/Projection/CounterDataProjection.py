import datetime

import pytz as pytz

from Projection.BaseProjection import BaseProjection


class CounterDataProjection(BaseProjection):
    def __init__(self,
                 database: str = "munich_bicycle",
                 srcCollection: str = "counter_data",
                 host: str = "localhost",
                 port: int = 27017,
                 dstCollection: str = "projection_counter_records",
                 user="",
                 password=""):
        super().__init__(database, srcCollection, host, port, dstCollection, user, password)

    def update(self):
        last_call = self.get_last_call()
        results = self.get_src_collection().find({"date": {"$gt": last_call["date"]}},
                                                 {"data.result.records": 1, "_id": 1, "date": 1})

        # print("start,end,startDate,endDate,date,counterposition,sum,1,2,dateResult,idResult")
        for result in results:
            month_of_record = result["date"]
            for record in result["data"]["result"]["records"]:
                date = self.get_date(month_of_record, record)

                start = self.get_time(date, record["uhrzeit_start"])
                end = self.get_time(date, record["uhrzeit_ende"])

                timezone = pytz.timezone("Europe/Berlin")

                # print(record["uhrzeit_start"]
                #       + "," + record["uhrzeit_ende"]
                #       + "," + start.strftime("%Y-%m-%dT%H:%M:%S")
                #       + "," + end.strftime("%Y-%m-%dT%H:%M:%S")
                #       + "," + record["datum"]
                #       + "," + record["zaehlstelle"]
                #       + "," + record["gesamt"]
                #       + "," + record["richtung_1"]
                #       + "," + record["richtung_2"]
                #       + "," + result["date"].strftime("%Y-%m-%dT%H:%M:%S")
                #       + "," + str(result["_id"]))

                dto = {
                    "date": start,
                    "dateEnd": end,
                    "dateUtc": start.astimezone(timezone),
                    "dateEndUtc": end.astimezone(timezone),
                    "sum": int(record["gesamt"]),
                    "direction_1": int(record["richtung_1"]),
                    "direction_2": int(record["richtung_2"]),
                    "counterposition": record["zaehlstelle"]
                }
                self.save(dto)

        self.close_connection()
        self.close_src_connection()

    def get_last_call(self):
        last_call = self.load_last()
        if last_call is None:
            last_call = {"date": datetime.datetime(self.START_YEAR, self.START_MONTH, 1)}
        return last_call
