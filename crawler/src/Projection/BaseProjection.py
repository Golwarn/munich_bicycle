import datetime

from pymongo import MongoClient

from BaseDatabase import BaseDatabase


class BaseProjection(BaseDatabase):
    def __init__(self,
                 database: str,
                 srcCollection: str,
                 host: str,
                 port: int,
                 dstCollection: str,
                 user: str,
                 password: str):
        super().__init__(database, dstCollection, host, port, user, password)
        self._srcCollection = srcCollection
        self.__password = password
        self.__srcClient = None

    def get_src_collection(self):
        db = self.get_src_database()
        collection = db[self._srcCollection]
        return collection

    def get_src_database(self):
        if self.__srcClient is None:
            con_string = self.get_connection_string(self._host, self._port, self._user, self.__password)
            self.__srcClient = MongoClient(con_string)
        db = self.__srcClient[self._database]
        return db

    def close_src_connection(self):
        if self.__srcClient is not None:
            self.__srcClient.close()
            self.__srcClient = None

    def save(self, data):
        collection = self.get_collection()
        collection.insert_one(data)

    def load_last(self):
        collection = self.get_collection()
        found = collection.find().sort(self.DATE, -1).limit(1)
        self.close_connection()
        if found.count() > 0:
            result = found[0]
            return result
        else:
            return None

    @staticmethod
    def get_date(month_of_record, record):
        date_record = record["datum"]
        date_split = date_record.split("T")[0].split("-")
        date_month = int(date_split[1])
        if date_month == month_of_record.month:
            date = datetime.date(month_of_record.year, date_month, int(date_split[2]))
        else:
            date = datetime.date(month_of_record.year, month_of_record.month, date_month)
        return date

    @staticmethod
    def get_time(date, time):
        time_start_split = time.split("T")
        time_start = datetime.datetime.strptime(time_start_split[1], "%H:%M:%S").time()
        return datetime.datetime.combine(date, time_start)
