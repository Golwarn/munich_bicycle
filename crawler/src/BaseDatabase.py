from pymongo import MongoClient


class BaseDatabase:
    START_MONTH = 12
    START_YEAR = 2016
    DATA = "data"
    TIMESTAMP = "timestamp"
    DATE = "date"

    def __init__(self, database: str, collection: str, host: str, port: int, user: str, password: str):
        self._database = database
        self._collection = collection
        self._host = host
        self._port = port
        self._user = user
        self.__password = password
        self._data = None
        self.__client = None

    def __del__(self):
        self.close_connection()

    def get_collection(self):
        db = self.get_database()
        collection = db[self._collection]
        return collection

    def get_database(self):
        if self.__client is None:
            con_string = self.get_connection_string(self._host, self._port, self._user, self.__password)
            self.__client = MongoClient(con_string)
        db = self.__client[self._database]
        return db

    def close_connection(self):
        if self.__client is not None:
            self.__client.close()
            self.__client = None

    def get_data(self):
        return self._data

    @staticmethod
    def get_connection_string(db_host, db_port, db_user, db_password):
        con_string = '{0}:{1}'.format(db_host, db_port)
        if db_user and db_password:
            con_string = 'mongodb://{0}:{1}@{2}'.format(db_user, db_password, con_string)
        # print("Connection String: " + con_string)
        return con_string
