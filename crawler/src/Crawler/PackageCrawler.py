import datetime

from Crawler.BaseCrawler import BaseCrawler
from Crawler.UrlEnding import UrlEnding


class PackageCrawler(BaseCrawler):
    COMBINATIONS = ["daten-der-raddauerzaehlstellen-muenchen-",
                    "daten-der-raddauerzaehlstellen-munchen-",
                    "daten-der-raddauerzahlstellen-munchen-",
                    "daten-der-raddauerzahlstellen-muenchen-"]
    URI = "uri"
    DATE = "date"
    MONTH = "month"
    YEAR = "year"

    def __init__(self,
                 database="munich_bicycle",
                 collectionPackage="package",
                 host="localhost",
                 port=27017,
                 user="",
                 password=""):
        super().__init__(database, collectionPackage, host, port, user, password)

    def curl(self, packageId: str):
        return super().curl("https://www.opengov-muenchen.de/api/3/action/package_show?id=" + packageId)

    def update(self, packages_data):
        last_call = self.get_last_call()
        available_resources = self.get_available_resources(packages_data)
        to_update_resources = self.get_to_update_resources(available_resources, last_call)
        return self.update_resources(to_update_resources)

    def update_resources(self, to_update_resources):
        updated = []
        collection = self.get_collection()
        for to_update in to_update_resources:
            print("Download package " + to_update.get_uri())
            data = self.curl(to_update.get_uri())
            if data is None:
                break
            now = datetime.datetime.now()
            result = {self.TIMESTAMP: now,
                      self.DATA: data,
                      self.DATE: to_update.get_date(),
                      self.URI: to_update.get_uri()}
            collection.insert_one(result)
            updated.append(to_update)
        self.close_connection()
        return updated

    @staticmethod
    def get_to_update_resources(available_resources, lastCall):
        last_update = lastCall["date"]
        to_update = []
        for available in available_resources:
            if last_update < available.get_date():
                to_update.insert(0, available)
            else:
                break
        return to_update

    def get_available_resources(self, packages_data):
        available_resources = []
        for value in packages_data["result"]:
            uri = value.strip()
            for starts in self.COMBINATIONS:
                if uri.startswith(starts):
                    available_resources.append(UrlEnding(uri))
        available_resources.sort(key=lambda url: url.get_date(), reverse=True)
        return available_resources
