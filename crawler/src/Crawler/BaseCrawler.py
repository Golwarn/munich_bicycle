import datetime
import json
from urllib.error import HTTPError
from urllib.request import urlopen

from BaseDatabase import BaseDatabase


class BaseCrawler(BaseDatabase):

    def __init__(self, database: str, collection: str, host: str, port: int, user: str, password: str):
        super().__init__(database, collection, host, port, user, password)

    def curl(self, path):
        print("curl: " + path)
        try:
            with urlopen(path) as url:
                self._data = json.loads(url.read().decode())
                return self._data
        except HTTPError as ex:
            print("HTTPError: " + ex.reason)
            return None

    def save(self, data=None):
        dataTmp = data
        if dataTmp is None:
            dataTmp = self._data
        if dataTmp is not None:
            collection = self.get_collection()
            now = datetime.datetime.now()
            result = {self.TIMESTAMP: now, self.DATA: dataTmp}
            collection.insert_one(result)

    def load_last(self):
        collection = self.get_collection()
        found = collection.find().sort(self.TIMESTAMP, -1).limit(1)
        self.close_connection()
        if found.count() > 0:
            result = found[0]
            self._data = result["data"]
            return result
        else:
            return None

    def get_last_call(self):
        last_call = self.load_last()
        if last_call is None:
            last_call = {"date": datetime.datetime(self.START_YEAR, self.START_MONTH, 1)}
        return last_call
