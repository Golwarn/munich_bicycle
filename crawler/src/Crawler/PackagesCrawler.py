import datetime

from Crawler.BaseCrawler import BaseCrawler


class PackagesCrawler(BaseCrawler):

    def __init__(self,
                 database="munich_bicycle",
                 collection="packages",
                 host="localhost",
                 port=27017,
                 user="",
                 password=""):
        super().__init__(database, collection, host, port, user, password)

    def curl(self):
        return super().curl("https://www.opengov-muenchen.de/api/3/action/package_list")

    def save(self, data, old):
        collection = self.get_collection()
        now = datetime.datetime.utcnow()
        if old is not None:
            collection.update_one({"_id": old["_id"]}, {"$set": {"timestamp": now, "data": data}})
        else:
            result = {"timestamp": now, "data": data}
            collection.insert_one(result)
        self.close_connection()

    def update(self):
        old = self.load_last()
        print("Download packages")
        data = self.curl()
        self._data = data
        if (old is not None and data != old["data"]) or old is None:
            self.save(data, old)

    def get_last_call(self):
        return self.load_last()
