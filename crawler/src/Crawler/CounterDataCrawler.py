from Crawler.DataCrawler import DataCrawler


class CounterDataCrawler(DataCrawler):

    def __init__(self,
                 database="munich_bicycle",
                 collection="counter_data",
                 host="localhost",
                 port=27017,
                 user="",
                 password="",
                 packageCollection="package"):
        super().__init__("15 Minuten", 20000, database, collection, host, port, packageCollection, user, password)
