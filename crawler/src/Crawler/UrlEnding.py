import datetime


class UrlEnding:
    MONTH = {"januar": "01", "februar": "02", "maerz": "03", "april": "04", "mai": "05", "juni": "06", "juli": "07",
             "august": "08",
             "september": "09", "oktober": "10", "november": "11", "dezember": "12"}

    def __init__(self, uri):
        self.__uri = uri
        sentences = uri.split("-")
        self.__month = sentences[-2]
        self.__year = sentences[-1]
        self.__date = datetime.datetime(int(self.__year), int(self.MONTH[self.__month]), 1)

    def get_year(self):
        return self.__year

    def get_month(self):
        return self.__month

    def get_date(self):
        return self.__date

    def get_uri(self):
        return self.__uri

    def __repr__(self):
        return self.__date.__repr__()
