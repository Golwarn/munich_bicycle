from Crawler.DataCrawler import DataCrawler


class WeatherDataCrawler(DataCrawler):

    def __init__(self,
                 database="munich_bicycle",
                 collection="weather_data",
                 host="localhost",
                 port=27017,
                 user="",
                 password="",
                 packageCollection="package"):
        super().__init__("Tageswerte", 250, database, collection, host, port, packageCollection, user, password)
