import datetime

from Crawler.BaseCrawler import BaseCrawler


class DataCrawler(BaseCrawler):

    def __init__(self,
                 resourceName,
                 limit,
                 database="munich_bicycle",
                 collection="counter_data",
                 host="localhost",
                 port=27017,
                 packageCollection="package",
                 user="",
                 password=""):
        super().__init__(database, collection, host, port, user, password)
        self.__resourceName = resourceName
        self.__limit = limit
        self.__packageCollection = packageCollection

    def curl(self, resourceId: str, limit: int):
        url = "https://www.opengov-muenchen.de/api/action/datastore_search?resource_id=" + resourceId + "&limit=" + str(
            self.__limit)
        return super().curl(url)

    def update(self):
        db = self.get_database()
        last_call = self.get_last_call()
        print("lastCall: " + str(last_call["date"]))
        package_collection = db[self.__packageCollection]
        result = package_collection.find({"date": {"$gt": last_call["date"]}},
                                        {"date": 1,
                                         "_id": 0,
                                         "data.result.resources.name": 1,
                                         "data.result.resources.id": 1})

        collection = self.get_collection()
        for row in result:
            resources = row["data"]["result"]["resources"]
            for resource in resources:
                print(resource["name"])
                if resource["name"].strip().startswith(self.__resourceName):
                    resource_id = resource["id"]
                    date = row["date"]
                    print("Download " + self._collection + " from " + str(date))
                    data = self.curl(resource_id, 20000)
                    if data is None:
                        self.close_connection()
                        return
                    now = datetime.datetime.now()
                    result = {self.TIMESTAMP: now, self.DATA: data, "date": date, "resource_id": resource_id}
                    collection.insert_one(result)
                    break
        self.close_connection()
