import json


class Values:

    def __init__(self,
                 period,
                 start,
                 end,
                 min_start,
                 max_end,
                 graph_type,
                 cyclists: bool,
                 min_temp: bool,
                 max_temp: bool,
                 locations,
                 location: str):
        self.period = period
        self.start = start
        self.end = end
        self.min_start = min_start
        self.max_end = max_end
        self.graph_type = graph_type
        self.cyclists = cyclists
        self.min_temp = min_temp
        self.max_temp = max_temp
        self.locations = locations
        self.location = location

    def __str__(self):
        return json.dumps(self.__dict__)
