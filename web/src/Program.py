import datetime
import io
import json
import os

import matplotlib.dates as mdates
from flask import Flask, render_template, Response, request
from flask_babel import Babel, gettext
from matplotlib.figure import Figure
from pymongo import MongoClient

from Values import Values

print("Server started")

db_port = 27017
if "DB_PORT" in os.environ:
    db_port = os.environ["DB_PORT"]
db_host = "localhost"
if "DB_HOST" in os.environ:
    db_host = os.environ["DB_HOST"]
db_user = ""
if "DB_USER" in os.environ:
    db_user = os.environ["DB_USER"]
db_password = ""
if "DB_PASS" in os.environ:
    db_password = os.environ["DB_PASS"]
db_name = "munich_bicycle"
if "DB_NAME" in os.environ:
    db_name = os.environ["DB_NAME"]

app = Flask(__name__)
app.config.from_pyfile('app.config')
babel = Babel(app)


def close_running_threads():
    print("Threads complete, ready to finish")


def get_connection_string():
    con_string = '{0}:{1}'.format(db_host, db_port)
    if db_user and db_password:
        con_string = 'mongodb://{0}:{1}@{2}'.format(db_user, db_password, con_string)
    return con_string


@app.route("/", methods=['GET', 'POST'])
def index():
    client = MongoClient(get_connection_string())
    db = client[db_name]
    collection = db["projection_counter_records"]
    pipeline = [{"$sort": {"_id": 1}},
                {"$group": {"_id": None, "first": {"$first": '$date'}, "last": {"$last": '$date'}}}
                ]
    date_range = list(collection.aggregate(pipeline=pipeline))
    client.close()
    print("available range: " + str(date_range))
    location_collection = db["projection_weather_records"]
    locations = location_collection.distinct("counterposition")

    if len(date_range) > 0:
        min_start = date_range[0]["first"].strftime('%Y-%m-%d')
        max_end = date_range[0]["last"].strftime('%Y-%m-%d')
        period = request.form.get("period", "Monthly")
        start = request.form.get("start", min_start)
        end = request.form.get("end", max_end)
        graph_type = request.form.get("graph_type", "Normal")
        print('form')
        print(request.form)
        plots_cyclists = request.form.get("cyclists", False, bool)
        plots_min_temp = request.form.get("min_temp", False, bool)
        plots_max_temp = request.form.get("max_temp", False, bool)
        location = request.form.get("location", "All", str)

        values = Values(period, start, end, min_start, max_end, graph_type, plots_cyclists, plots_min_temp,
                        plots_max_temp, locations, location)
    else:
        print("No data in database found")
        values = Values("Monthly", "2018-01-01", "2018-12-31", "2018-01-01", "2018-12-31", "Normal", True, True, True,
                        locations, "All")
    return render_template("index.html", values=values)


def get_counter_axis_data(inputs):
    results = get_counter_data(inputs)
    xs = []
    ys = []
    if inputs["period"] == "Weekday" and inputs['graph_type'] == "Average":
        weekday_sum = {}
        weekday_counter = {}
        # cheat for design
        dates = {0: datetime.datetime(2020, 1, 25),
                 1: datetime.datetime(2020, 1, 26),
                 2: datetime.datetime(2020, 1, 27),
                 3: datetime.datetime(2020, 1, 28),
                 4: datetime.datetime(2020, 1, 29),
                 5: datetime.datetime(2020, 1, 30),
                 6: datetime.datetime(2020, 1, 31)}

        for day in range(0, 7):
            weekday_sum[day] = 0
            weekday_counter[day] = 0

        for result in results:
            weekday = result["_id"].weekday()
            weekday_counter[weekday] = weekday_counter[weekday] + 1
            weekday_sum[weekday] = weekday_sum[weekday] + result["sum"]

        for day in range(0, 7):
            xs.append(dates[day])
            ys.append(weekday_sum[day] / weekday_counter[day])

    else:
        for result in results:
            ys.append(result["sum"])
            xs.append(result["_id"])
    return xs, ys


@app.route("/weather/<values>")
def weather(values):
    lns_counter = None
    lns_weather_max = None
    lns_weather_min = None
    date_min = None
    date_max = None
    counter_axis = None
    weather_axis = None
    inputs = json.loads(values)
    fig = Figure(figsize=[20, 5])

    if inputs['cyclists']:
        counter_xs, counter_ys = get_counter_axis_data(inputs)

        # round to nearest years.
        date_min = counter_xs[0]
        date_max = counter_xs[-1]
        counter_axis = fig.add_subplot(1, 1, 1)
        lns_counter = counter_axis.plot(counter_xs, counter_ys, color='black', label=gettext(u'Cyclists'))
        counter_axis.set_ylabel(gettext(u'Counter of cyclists'))

        set_axis_labels(counter_axis, inputs)
        counter_axis.set_xlim(date_min, date_max)
        counter_axis.grid(True)

    if inputs['min_temp'] or inputs['max_temp']:
        max_temp, min_temp, weather_xs = get_weather_axis_data(inputs)
        if inputs['cyclists']:
            weather_axis = counter_axis.twinx()
            weather_axis.grid(False)
        else:
            # round to nearest years.
            date_min = weather_xs[0]
            date_max = weather_xs[-1]
            weather_axis = fig.add_subplot(1, 1, 1)
            weather_axis.grid(True)
        weather_axis.set_ylabel(gettext(u'Temperature in °Celsius'))

        set_axis_labels(weather_axis, inputs)
        weather_axis.set_xlim(date_min, date_max)

        if inputs['min_temp']:
            lns_weather_min = weather_axis.plot(weather_xs, min_temp, color='blue', label=gettext(u"Min Temperature"))
        if inputs['max_temp']:
            lns_weather_max = weather_axis.plot(weather_xs, max_temp, color='red', label=gettext(u"Max Temperature"))

    lns = get_lns(inputs, lns_counter, lns_weather_max, lns_weather_min)
    print_legend(inputs, lns, counter_axis, weather_axis)

    output = io.BytesIO()
    fig.savefig(output, format="svg")
    return Response(output.getvalue(), mimetype='image/svg+xml')


def print_legend(inputs, lns, counter_axis, weather_axis):
    if lns is not None:
        labs = [lab.get_label() for lab in lns]
        if inputs['cyclists']:
            counter_axis.legend(lns, labs, loc=0)
        else:
            weather_axis.legend(lns, labs, loc=0)


def get_lns(inputs, lns_counter, lns_weather_max, lns_weather_min):
    if inputs['cyclists'] and inputs['min_temp'] and inputs['max_temp']:
        lns = lns_counter + lns_weather_min + lns_weather_max
    elif inputs['min_temp'] and inputs['max_temp']:
        lns = lns_weather_min + lns_weather_max
    elif inputs['cyclists'] and inputs['max_temp']:
        lns = lns_counter + lns_weather_max
    elif inputs['cyclists'] and inputs['min_temp']:
        lns = lns_counter + lns_weather_min
    elif inputs['cyclists']:
        lns = lns_counter
    elif inputs['min_temp']:
        lns = lns_weather_min
    elif inputs['max_temp']:
        lns = lns_weather_max
    else:
        lns = None
    return lns


def get_weather_axis_data(inputs):
    results = get_weather_data(inputs)
    xs = []
    min_temp = []
    max_temp = []
    min_max = []
    for result in results:
        min_temp.append(result["minTemp"])
        max_temp.append(result["maxTemp"])
        min_max.append([result["minTemp"], result["maxTemp"]])
        xs.append(result["_id"])
    return max_temp, min_temp, xs


def set_axis_labels(axis, inputs):
    period = inputs['period']

    if period == "Weekday" and inputs['graph_type'] == "Average":
        axis.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=0))
        axis.xaxis.set_major_formatter(mdates.DateFormatter('%a'))
        axis.xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=0))
        axis.xaxis.set_minor_formatter(mdates.DateFormatter('%a'))

    # x_days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    # x = [1,2,3,4,5,6,7]
    # y = [1,2,3,4,5,6,7]
    #
    # plt.xticks(x, x_days)
    # plt.plot(x, y)
    # plt.show()
    # elif period == "Weekly":
    #     axis.xaxis.set_major_locator(mdates.WeeklyLocator())
    #     axis.xaxis.set_major_formatter(mdates.DateFormatter('%a'))
    else:
        major_locator = mdates.AutoDateLocator()
        axis.xaxis.set_major_locator(major_locator)
        axis.xaxis.set_major_formatter(mdates.ConciseDateFormatter(major_locator))
        if period != "Weekday":
            axis.xaxis.set_minor_locator(mdates.AutoDateLocator())
        else:
            axis.xaxis.set_minor_locator(mdates.WeekdayLocator(byweekday=0))
            axis.xaxis.set_minor_formatter(mdates.DateFormatter('%d\n%a'))


def get_counter_data(inputs):
    client = MongoClient(get_connection_string())
    db = client[db_name]

    collection = get_counter_collection(db)

    start = string_to_date(inputs['start'])
    end = string_to_date(inputs['end']) + datetime.timedelta(1)
    pipeline = get_counter_pipeline(end, start, inputs)
    results = list(collection.aggregate(pipeline=pipeline))

    client.close()
    return results


def get_weather_data(inputs):
    client = MongoClient(get_connection_string())
    db = client[db_name]

    collection = get_weather_collection(db)

    start = string_to_date(inputs['start'])
    end = string_to_date(inputs['end']) + datetime.timedelta(1)
    pipeline = get_weather_pipeline(end, start, inputs)
    results = list(collection.aggregate(pipeline=pipeline))

    client.close()
    return results


def get_counter_pipeline(end, start, inputs):
    period = inputs["period"]
    if period == "Monthly":
        group_id = {
            '$dateFromString': {
                'dateString': {
                    '$dateToString': {
                        'format': '%Y-%m',
                        'date': '$date'
                    }
                }
            }
        }
    elif period == "Daily" or period == "Weekday":
        group_id = {
            '$dateFromString': {
                'dateString': {
                    '$dateToString': {
                        'format': '%Y-%m-%d',
                        'date': '$date'
                    }
                }
            }
        }
    else:
        group_id = {
            '$dateFromString': {
                'format': '%Y-%m-%dT%H',
                'dateString': {
                    '$dateToString': {
                        'format': '%Y-%m-%dT%H',
                        'date': '$date'
                    }
                }
            }
        }

    counter_filter = {
        'date': {
            '$gte': start
        },
        'dateEnd': {
            '$lt': end
        }
    }
    location = inputs["location"]
    if location != "All":
        counter_filter["counterposition"] = location
    pipeline = [
        {
            '$match': counter_filter
        }, {
            '$group': {
                '_id': group_id,
                'sum': {
                    '$sum': '$sum'
                    # },
                    # 'sum_direction_1': {
                    #     '$sum': '$direction_1'
                    # },
                    # 'sum_direction_2': {
                    #     '$sum': '$direction_2'
                }
            }
        }, {
            '$sort': {
                '_id': 1
            }
        }
    ]
    # print("counter-pipeline: " + str(pipeline))
    return pipeline


def get_weather_pipeline(end, start, inputs):
    period = inputs["period"]
    if period == "Monthly":
        group_id = {
            '$dateFromString': {
                'dateString': {
                    '$dateToString': {
                        'format': '%Y-%m',
                        'date': '$date'
                    }
                }
            }
        }
    else:
        group_id = {
            '$dateFromString': {
                'dateString': {
                    '$dateToString': {
                        'format': '%Y-%m-%d',
                        'date': '$date'
                    }
                }
            }
        }

    weather_filter = {
        'date': {
            '$gte': start,
            '$lte': end
        }
    }

    location = inputs["location"]
    if location != "All":
        weather_filter["counterposition"] = location
    pipeline = [
        {
            '$match': weather_filter
        }, {
            '$group': {
                '_id': group_id,
                'minTemp': {
                    '$min': '$minTemp'
                },
                'maxTemp': {
                    '$max': '$maxTemp'
                }
            }
        }, {
            '$sort': {
                '_id': 1
            }
        }
    ]
    # print("weather-pipeline: " + str(pipeline))
    return pipeline


def string_to_date(date):
    date2 = datetime.datetime.strptime(date, '%Y-%m-%d')
    return datetime.datetime(date2.year, date2.month, date2.day, date2.hour, date2.minute, date2.second,
                             tzinfo=datetime.timezone.utc)


def get_counter_collection(db):
    return db["projection_counter_records"]


def get_weather_collection(db):
    return db["projection_weather_records"]


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())


if __name__ == '__main__':
    app.run()
