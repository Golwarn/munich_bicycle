# munich_bicycle

This project is for visualisation the data of the bicycle counter of Munich. It is still under construction. The second reason is to practise my python skills.

This project is written in [Python3](https://www.python.org/) and is under [MIT-License](https://gitlab.com/Golwarn/munich_bicycle/-/blob/master/LICENSE).

I want to try it without javascript, don't blame me for that, please.

# Requirements
- The application is running with [Python3](https://www.python.org/) or [Docker](https://www.docker.com/)
- The database in the background is [MongoDB](https://www.mongodb.com)

# Run

## Database

For docker:
```
docker run -d --name "mongodb" mongo
```

## Crawler

Get Help:
```
py Crawler.py -h
```
Or use [Docker](https://gitlab.com/Golwarn/munich_bicycle/-/blob/master/crawler/Dockerfile):
```
docker build --tag mb-crawler . 
docker run --name crawler mb-crawler
```

The first run takes some minutes because of the first of data. Don't stop until it says it is done.

## Website
Have a look on the original documentation of flask how to start the webserver: https://flask.palletsprojects.com/en/1.1.x/quickstart/

Or use [Docker](https://gitlab.com/Golwarn/munich_bicycle/-/blob/master/web/Dockerfile):
```
docker build --tag mb-web . 
docker run -d --name web mb-web
```
